package com.company;

import java.util.Objects;

public class Tri {
    private int height;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setHeight(int height) {
        this.height = height;
    }



    public int getHeight() {
        return height;

    }

    @Override
    public String toString() {
        return "Tri{" +
                "height=" + height +
                ", name='" + name + '\'' +
                '}';
    }
}
